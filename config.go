package config

import (
	"strings"

	"github.com/spf13/viper"
)

//
// ====== Setup Config ======
//

// func setDefault() {
// 	viper.SetDefault("set.default", "example_set_default")
// }

func loadDefault(configPath []string) {
	viper.SetConfigName("default") // name of config file (without extension)
	if configPath != nil {
		viper.AddConfigPath(configPath[0]) // custom path to look for the config file in
	}
	viper.AddConfigPath("./configs") // 1st path to look for the config file in
	viper.AddConfigPath(".")         // 2nd path to look for the config file in
	_ = viper.ReadInConfig()         // Find and read the config file
}

func Set(prefix string, configPath ...string) {
	loadDefault(configPath) // Load from file
	viper.SetEnvPrefix(prefix)
	viper.AutomaticEnv() // read in environment variables that match
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
}

//
// ====== Function for reading config ====== //Some of them are not using in ZEGAL yet
//

// Get(key string) : interface{}
// func Get(key string) interface{} { return viper.Get(key) }

// GetBool(key string) : bool
func GetBool(key string) bool { return viper.GetBool(key) }

// GetFloat64(key string) : float64
func GetFloat64(key string) float64 { return viper.GetFloat64(key) }

// GetInt(key string) : int
func GetInt(key string) int { return viper.GetInt(key) }

// GetString(key string) : string
func GetString(key string) string { return viper.GetString(key) }

// GetStringMap(key string) : map[string]interface{}
// func GetStringMap(key string) map[string]interface{} { return viper.GetStringMap(key) }

// GetStringMapString(key string) : map[string]string
func GetStringMapString(key string) map[string]string { return viper.GetStringMapString(key) }

// GetStringSlice(key string) : []string
func GetStringSlice(key string) []string { return viper.GetStringSlice(key) }

// GetTime(key string) : time.Time
// func GetTime(key string) time.Time { return viper.GetTime(key) }

// GetDuration(key string) : time.Duration
// func GetDuration(key string) time.Duration { return viper.GetDuration(key) }

// IsSet(key string) : bool
func IsSet(key string) bool { return viper.IsSet(key) }

// viper.ConfigFileUsed()
func ConfigFileUsed() string { return viper.ConfigFileUsed() }
